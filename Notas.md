
# Configurando gitlab runner 


```shell
#baixando a imagem
docker pull gitlab/gitlab-runner:latest

#rodando o container
docker run -d --name gitlab-runner --restart always -v /Users/Shared/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

# acessando o container para configurar o token 
docker exec -it gitlab-runner bash

# comando para registrar o runner 

````

- É necessario registrar o runner para isso va em gitlab.com -> settings -> CI/CD -> Runners 

